-------------------------------------------------------------------------------
SCORPIO
Scalable Parallel I/O module for Environmental Management Applications
-------------------------------------------------------------------------------
This library provides software that read/write data sets from/to parallel file 
systems in an efficient and scalable manner. 
In this context, scalable means that the simulators read/write 
performance does not degrade significantly as the number of cores grows.

-------------------------------------------------------------------------------
COPYRIGHT AND LICENSE 
-------------------------------------------------------------------------------
SCORPIO is distrubuted under the terms of the GNU Lesser General Public 
License (LGPL). The copyright is held jointly by North Carolina State University 
and Pacific Northwest National Laboratory. 

The copyright and license information is specified in the included file 
COPYRIGHT. 

-------------------------------------------------------------------------------
Repository Access
-------------------------------------------------------------------------------
Please request access by contacting Sarat Sreepathi (admin@sarats.com). 
Repository located at:
https://bitbucket.org/sarats/scorpio


-------------------------------------------------------------------------------
Building Library
-------------------------------------------------------------------------------
The current stable release is 2.2.

--------------
Prerequisites:
--------------
MPI
C compiler
HDF5 libraries (preferably with parallel(MPI) support)
Optional: Fortran (for Fortran example)

After downloading SCORPIO and gathering details of HDF5 installation, 
the following commands can be used to build and install SCORPIO: 

	cd <SCORPIO check out directory>/src
	make CC=<C-compiler> HDF5_INCLUDE_DIR=<location of the HDF5 include directory>
	make SCORPIO_INSTALL_DIR=<user defined install location> install

In this case, CC refers to C compiler with MPI support, e.g., mpicc.

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------