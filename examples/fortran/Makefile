# ******************************************************************************
# Copyright Notice
#  + 2010-2012 North Carolina State University
#  + 2010-2012 Pacific Northwest National Laboratory
# 
# This file is part of SCORPIO.
# 
# SCORPIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# SCORPIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with SCORPIO.  If not, see <http://www.gnu.org/licenses/>.
# 
# ******************************************************************************
#  @file Makefile
#  @brief Makefile for testing SCORPIO parallel I/O library
#  @author Sarat Sreepathi
#  @version 1.1p
#  @date 2011-09-19

# Set to -O3 if compiler flags are not set 
ifndef CFLAGS
CFLAGS += -O3
endif

ifndef FFLAGS
FFLAGS += -O3
endif

# # ***************************************************************************
# Mandatory arguments to build test program:
# FC, HDF5_INCLUDE_DIR, HDF5_LIB_DIR (or HDF5_LD_FLAGS) and SCORPIO_DIR (for linking with library)
# e.g., make FC=ftn HDF5_INCLUDE_DIR=<path> HDF5_LIB_DIR=<path> SCORPIO_DIR=<path> 
# Alternately, you can specify HDF5_LD_FLAGS too if you prefer more control.
# e.g., make CC=cc HDF5_INCLUDE_DIR=<path> SCORPIO_DIR=<path> HDF5_LD_FLAGS="-L<path> -lhdf5 -lz"
# # ***************************************************************************

# If HDF5 include dir is given, pass to FFLAGS 
ifdef HDF5_INCLUDE_DIR
FFLAGS += -I$(HDF5_INCLUDE_DIR)
endif

# Need to updated include and LDFLAGS using SCORPIO_DIR
ifdef SCORPIO_DIR
FFLAGS += -I$(SCORPIO_DIR)/include 
LDFLAGS += -L$(SCORPIO_DIR)/lib -lscorpio
endif

# If HDF5_LIB_DIR is set, add that to LDFLAGS
ifdef HDF5_LIB_DIR
LDFLAGS += -L$(HDF5_LIB_DIR) -lhdf5 -lz
endif

# If HDF5_LIB_DIR is set, add that to LDFLAGS
ifdef HDF5_LD_FLAGS
LDFLAGS += $(HDF5_LD_FLAGS)
endif

# Alternately you can specify a platform details by creating a new machine section
MACHINE=titan
#MACHINE=hopper

ifeq ($(MACHINE),hopper)
	CC=cc
	FC=ftn
	LINKER=ftn
	CFLAGS += -O3 
	FFLAGS += -O3 
	LDFLAGS += -lhdf5 -lz -L../../lib -lscorpio
	# module load hdf5-parallel already adds this to the library path
	# LDFLAGS= -L$(CRAY_HDF5_DIR)/hdf5-parallel-pgi/lib -lhdf5 -lz
endif

ifeq ($(MACHINE),cygnus)
	CC=mpicc
	FC=mpif90
	LINKER=mpif90
	CFLAGS+= -O3 
	FFLAGS+= -O3 
	LDFLAGS+= -Wl,-L/soft/hdf5/default/lib -lhdf5 -lz -L../../lib -lscorpio
endif

ifeq ($(MACHINE),optimus)
	CC=mpicc
	FC=mpif90
	LINKER=mpif90
	CFLAGS+= -O3 -I${SCORPIO_DIR}/include
	FFLAGS+= -O3 -I${SCORPIO_DIR}/include
	LDFLAGS+= -L/opt/hdf5/default -lhdf5 -lz -L${SCORPIO_DIR}/lib -lscorpio
endif


ifeq ($(MACHINE),titan)
	CC=cc
	FC=ftn
	LINKER=ftn
	CFLAGS+= -O3 -I${SCORPIO_DIR}/include
	FFLAGS+= -O3 -I${SCORPIO_DIR}/include
	LDFLAGS+= -lhdf5 -lz -L${SCORPIO_DIR}/lib -lscorpio
endif

OBJS:=$(patsubst %.c,%.o,$(wildcard *.c))
OBJS+=$(patsubst %.f90,%.o,$(wildcard *.f90))
EXE:=test

all:$(EXE) 

$(EXE):$(OBJS)
	$(FC) $(FFLAGS) $(OBJS) -o $(EXE) $(LDFLAGS) 

clean:
	\rm -f $(OBJS) $(EXE)

.SUFFIXES: .cpp .c .h .o .hpp .f90

.cpp.o:
	$(CPP) $(CPPFLAGS) -c $< -o $@

.f90.o:
	$(FC) $(FFLAGS) -c $< -o $@

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

