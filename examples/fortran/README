-------------------------------------------------------------------------------
SCORPIO test program 
-------------------------------------------------------------------------------
This test reads a simple HDF5 file with two datasets and writes it back for 
verification. 

It illustrates using the NONUNIFORM_CONTIGUOUS_WRITE and NONUNIFORM_CONTIGUOUS_READ
patterns. 

-------------------------------------------------------------------------------
Building 
-------------------------------------------------------------------------------
You need to first build the SCORPIO library. The current stable release is 2.1.  

--------------
Prerequisites:
--------------
MPI
C,Fortran compilers
HDF5 libraries (preferably with parallel(MPI) support)

----------------
SCORPIO library
----------------
After downloading SCORPIO and gathering details of HDF5 installation, 
the following commands can be used to build and install SCORPIO library: 

	cd <SCORPIO check out directory>/src
	make CC=<C-compiler> HDF5_INCLUDE_DIR=<location of the HDF5 include directory>
	make SCORPIO_INSTALL_DIR=<user defined install location> install

In this case, CC refers to C compiler with MPI support, e.g., mpicc.

----------------
Building test
----------------
Mandatory arguments to build test program:
 CC, HDF5_INCLUDE_DIR, HDF5_LIB_DIR (or HDF5_LD_FLAGS) and SCORPIO_INSTALL_DIR (for linking with library)
 e.g., make FC=ftn HDF5_INCLUDE_DIR=<path> HDF5_LIB_DIR=<path> SCORPIO_INSTALL_DIR=<path> 
Use mpif90 or equivalent for FC.

Alternately, you can specify HDF5_LD_FLAGS too if you prefer more control.
 e.g., make FC=ftn HDF5_INCLUDE_DIR=<path> SCORPIO_INSTALL_DIR=<path> HDF5_LD_FLAGS="-L<path> -lhdf5 -lz"

-------------------------------------------------------------------------------
Run
-------------------------------------------------------------------------------
Command line arguments:
First argument is the scenario number 
  (1) IOGROUPS - only I/O processes read/write and 
  (2) ALLNODES - all processes read/write 
Second argument is number of I/O groups which is only relevant when using scenario 1.

Use 'mpirun -n <num_procs> ./test 1 1' to run a simple test.
In this case, the program creates one I/O group (one I/O master) to read input and write output file.
Experiment with different number of groups: e.g., for two groups - mpirun -n 4 ./test 1 2 

You can use mpiexec or equivalent command for your platform.

-------------------------------------------------------------------------------
Verify
-------------------------------------------------------------------------------
Use h5diff input.h5 output.h5 to verify that both files produce identical results.
Use 'h5dump output.h5' to print the contents of the file to screen. You can
manually verify that the correct number of rows exist in the file.

